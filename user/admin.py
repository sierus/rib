from django.contrib import admin

from user.models import *


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'role', 'last_name', 'first_name', 'middle_name', 'phone', 'fullpassport', 'turbo',
                    'birthday', 'is_active',
                    'is_superuser', 'is_staff', 'date_joined', 'last_login']
    list_display_links = ['role', 'last_name', 'first_name', 'middle_name', ]
    list_filter = ['role', 'is_active', ]
    search_fields = ['last_name', 'first_name', 'middle_name', 'username', 'phone', 'passport_seriya',
                     'passport_number', 'turbo', ]
    save_on_top = True

    # def get_img(self, obj):
    #     if obj.passport_photo:
    #         return mark_safe(f"<img src='{obj.passport_photo.url}' alt='passport_photo' width=50px>")
    #     else:
    #         pass

    def fullpassport(self, obj):
        if obj.passport_seriya:
            return f'{obj.passport_seriya}{obj.passport_number}'
        else:
            return '-'

    def save_model(self, request, obj, form, change):
        obj.set_password(obj.turbo)
        obj.username = obj.phone
        obj.save()
        return super().save_model(request, obj, form, change)


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    list_display_links = ['id', 'title']
    save_on_top = True


class DistrictInlineAdmin(admin.StackedInline):
    model = District
    extra = 10


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    list_display_link = ['id', 'title']
    save_on_top = True
    inlines = [DistrictInlineAdmin]


class QuarterAdmin(admin.StackedInline):
    model = Quarter
    extra = 10


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    list_display_link = ['id', 'title']
    save_on_top = True
    inlines = [QuarterAdmin]