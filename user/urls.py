from django.urls import path, include
from user.views import *


app_name = 'user'

urlpatterns = [
    # path('', ServicesList.as_view(), name='services_list'),
    path('login/', login_first, name='login_first'),
    path('is-register/', is_register, name='is_register'),
    path('signup/', user_signup, name='signup'),
    path('forgot-pass/', forgot_pass, name='forgot_pass'),
    path('logout/', user_logout, name='logout'),
    path('custom-logout/', Logout.as_view(), name='custom_logout'),

    #ajax
    path('get-district/', get_district, name='get_district'),
    path('get_quarter/', get_quarter, name='get_quarter'),
    path('get-code/', get_code, name='get_code'),

    path('save-user-information/', Save_User_Information.as_view(), name='save_user_information'),
    path('save-passport-data/', save_passport_data.as_view(), name='save_passport_data'),

]
