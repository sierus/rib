import datetime
import json
import random

import requests
from django.contrib import messages
from django.contrib.auth import authenticate, login, user_logged_out, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext_lazy as _

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views import View
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from rib.settings import PHONE_MAX_AGE, TOKEN_MAX_AGE
from user.models import *


def login_first(request):
    if request.method == 'POST':
        phone = request.POST.get('phone', None)
        password = request.POST.get('password', '')
        context = {
            'phone': phone
        }
        if len(str(phone)) != 9:
            try:
                phone = request.COOKIES['phone']
            except KeyError:
                return redirect(reverse_lazy('user:login_first'))
        try:
            user1 = User.objects.get(username=phone)
            if not user1.is_staff and user1.person_id == None:
                return redirect(reverse_lazy('user:signup'))
            user = authenticate(request, username=phone, password=password)

            if user is not None:
                login(request, user)
                response = redirect(reverse('service:applications_list'))
                return response
            else:
                messages.error(request, _("Login yoki parol noto'g'ri!"))
                context.update(redirect=True)
                return render(request, 'account/login.html', context)
        except ObjectDoesNotExist:
            response = redirect(reverse_lazy('user:signup'))
            response.set_cookie('phone', phone, max_age=PHONE_MAX_AGE)
            return response
    else:
        return render(request, 'account/login.html')


def is_register(request):
    if request.is_ajax():
        phone = request.GET.get('phone')
        try:
            user = User.objects.get(username=phone)
            if not user.is_staff and user.person_id == None:
                response = HttpResponse(False)
                response.set_cookie('phone', phone, max_age=PHONE_MAX_AGE)
                return response
            response = HttpResponse(True)
            response.set_cookie('phone', phone, max_age=PHONE_MAX_AGE)
            return response
        except ObjectDoesNotExist:
            response = HttpResponse(False)
            response.set_cookie('phone', phone, max_age=PHONE_MAX_AGE)
            return response


def user_signup(request):
    regions = Region.objects.all()
    print(regions)
    if not request.COOKIES.get('phone'):
        return redirect(reverse_lazy('user:login_first'))
    phone = request.COOKIES['phone']
    context = {
        'regions': regions,
        'phone': phone
    }
    return render(request, 'account/signup.html', context)


def forgot_pass(request):
    if request.is_ajax():
        phone = int(request.GET['phone'])
        try:
            user_pass = User.objects.get(phone=phone)
            user = User.objects.get(phone=phone)
            msg = f"E-RIB dasturi login va parolingiz:%0aLogin: {user.username} %0aParol: {user.turbo}"
            msg = msg.replace(" ", "+")
            # url = f"https://developer.apix.uz/index.php?app=ws&u={SMS_LOGIN}&h={SMS_TOKEN}&op=pv&to=998{phone}&msg={msg}"
            # response = requests.get(url)
            return HttpResponse(True)
        except:
            return HttpResponse(False)


@login_required
def user_logout(request):
    return render(request, 'account/logout.html')


class Logout(APIView):
    def get(self, request, format=None):
        # using Django logout
        logout(request)
        return redirect(reverse_lazy('user:login_first'))


def get_district(request):
    try:
        if request.is_ajax():
            districts = District.objects.filter(region=request.GET.get('region'))
            # options = "<option value=''>--- --- ---</option>"
            options = ""
            for district in districts:
                options += f"<option value='{district.id}'>{district.title}</option>"
            return HttpResponse(options, status=200)
        else:
            return HttpResponse(status=404)
    except:
        return HttpResponse(status=404)


def get_quarter(request):
    if request.is_ajax():
        quarters = Quarter.objects.filter(district=request.GET.get('district'))
        # options = "<option value=''>--- --- ---</option>"
        options = ""
        for quarter in quarters:
            options += f"<option value='{quarter.id}'>{quarter.title}</option>"
        return HttpResponse(options)
    else:
        return False


def get_code(request):
    if request.is_ajax():
        phone = request.GET.get('phone')
        try:
            user = User.objects.get(phone=phone)
            password = user.turbo
        except ObjectDoesNotExist:
            password = random.randint(10000, 99999)
            user = User.objects.create(username=phone, phone=phone, password=password, turbo=password)
            user.set_password(password)
            user.save()

        print(password)
        msg = f"E-RIB dasturidan ro'yhatdan o'tishni yakunlash va tizimga kirish ma'lumotlari  %0aLogin: {user.username} %0aParol: {user.turbo}"
        msg = msg.replace(" ", "+")
        # url = f"https://developer.apix.uz/index.php?app=ws&u={SMS_LOGIN}&h={SMS_TOKEN}&op=pv&to=998{user.phone}&msg={msg}"
        # response = requests.get(url)

        context = {
            'password': password
        }
        data = json.dumps(context)
        response = HttpResponse(data, content_type='json')
        return response
    else:
        return HttpResponse(False)


class Save_User_Information(View):
    def post(self, request):

        if request.is_ajax():
            last_name = request.POST.get('last_name')
            first_name = request.POST.get('first_name')
            middle_name = request.POST.get('middle_name')
            birthday = datetime.datetime.strptime(request.POST.get('birthday'), '%d.%m.%Y')
            region = request.POST.get('region')
            district = request.POST.get('district')
            quarter = request.POST.get('quarter')
            address = request.POST.get('address')
            phone = request.POST.get('phone')
            user = User.objects.get(phone=phone)
            if user is not None:
                user.last_name = last_name
                user.first_name = first_name
                user.middle_name = middle_name
                user.birthday = birthday
                user.region.id = region
                user.district.id = district
                user.quarter.id = quarter
                user.address = address
                user.save()
                context = {
                    'user': user.id
                }
                data = json.dumps(context)
                return HttpResponse(data, content_type='json')
            else:
                return HttpResponse(False)
        else:
            return HttpResponse(False)


@permission_classes([IsAuthenticated])
class save_passport_data(APIView):
    def post(self, request):
        if request.is_ajax():
            user = get_object_or_404(User, id=request.POST.get('user_id'))
            if user is not None:
                user.passport_seriya = request.POST.get('passport_seriya')
                user.passport_number = request.POST.get('passport_number')
                user.issue_by_whom = request.POST.get('issue_by_whom')
                user.person_id = request.POST.get('person_id')
                user.save()

                user = authenticate(request, username=user.phone, password=user.turbo)
                login(request, user)
                return HttpResponse(True)
            else:
                return HttpResponse(False)
        else:
            return HttpResponse(False)
