from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.db import models
from django.http import Http404
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Region(models.Model):
    title = models.CharField(verbose_name=_('Nomi'), max_length=255)
    sort = models.IntegerField(blank=True, default=1)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['sort']
        verbose_name = _('Viloyat')
        verbose_name_plural = _('Viloyatlar')


class District(models.Model):
    title = models.CharField(verbose_name=_('Nomi'), max_length=255)
    region = models.ForeignKey(Region, verbose_name=_('Viloyat'), on_delete=models.SET_NULL, null=True)
    sort = models.IntegerField(blank=True, default=1)
    is_active = models.BooleanField(verbose_name="Moderatorlar tasdiqlagan", default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['sort', ]
        verbose_name = _('Tuman/Shahar')
        verbose_name_plural = _('Tumanlar/Shaharlar')


class Quarter(models.Model):
    title = models.CharField(verbose_name=_('Nomi'), max_length=255)
    district = models.ForeignKey(District, verbose_name=_('Tuman/Shahar'), on_delete=models.SET_NULL, null=True)
    sort = models.IntegerField(blank=True, default=1)
    is_active = models.BooleanField(verbose_name="Moderatorlar tasdiqlagan", default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['sort', ]
        verbose_name = _('Mahalla')
        verbose_name_plural = _('Mahallalar')


class Department(models.Model):
    parent = models.ForeignKey('self', verbose_name=_("Bo'ysinuvchi tashkilot"), on_delete=models.CASCADE, null=True,
                               blank=True)
    title = models.CharField(max_length=300, verbose_name=_("Bo'lim nomi"), blank=True, null=True)
    region = models.ForeignKey(Region, verbose_name=_('Viloyat'), on_delete=models.SET_NULL, null=True, blank=True)
    district = models.ManyToManyField(District, verbose_name=_('Tuman/Shahar'), blank=True)
    is_active = models.BooleanField(verbose_name=_("Faolligi"), default=True)
    pay_for_service = models.BooleanField(verbose_name=_("Xizmat uchun to'lov olish"), default=True)
    longitude = models.CharField(_("Kordinata Karta  longitude"), max_length=20, blank=True)
    latitude = models.CharField(_("Kordinata Karta latitude"), max_length=20, blank=True)

    class Meta:
        verbose_name = _("Bo'lim")
        verbose_name_plural = _("Bo'limlar")

    def __str__(self):
        return f"{self.region.title}: {self.title}"


PHYSICAL_PERSON = 0  # Jismoniy shaxs
LEGAL_PERSON = 1  # Yuridik shaxs

PERSON_TYPE_CHOICES = (
    (PHYSICAL_PERSON, _('Jismoniy shaxs')),
    (LEGAL_PERSON, _('Yuridik shaxs')),
)

USER = '1'
CHECKER = '2'
REVIEWER = '3'
TECHNICAL = '4'
DISTRICAL_CONTROLLER = '5'
REGIONAL_CONTROLLER = '6'
STATE_CONTROLLER = '7'
OPERATOR = '8'
MODERATOR = '9'
ADMINISTRATOR = '10'
SUPER_ADMINISTRATOR = '11'

ROLE = (
    (USER, _('Oddiy foydalanuvchi')),
    (CHECKER, _('Arizalarni tekshiruvchi xodim')),  # OPERATOR
    (REVIEWER, _('Ma\'lumotlar mosligini tasdiqlovchi xodim')),
    (TECHNICAL, _('Texnik ko\'rik o\'tkazuvchi xodim')),
    (DISTRICAL_CONTROLLER, _('Tuman nazoratchisi')),
    (REGIONAL_CONTROLLER, _('Viloyat nazoratchisi')),
    (STATE_CONTROLLER, _('Davlat nazoratchisi')),
    (OPERATOR, _('Operator')),
    (MODERATOR, _('Moderator')),
    (ADMINISTRATOR, _('Administrator')),
    (SUPER_ADMINISTRATOR, _('Super administrator')),

)

MEN = 0
WOMEN = 1

GENDER_CHOICES = (
    (MEN, 'Erkak'),
    (WOMEN, 'Ayol'),
)


class User(AbstractBaseUser, PermissionsMixin):
    last_name = models.CharField(verbose_name=_('Familiya'), max_length=255, blank=True)
    first_name = models.CharField(verbose_name=_('Ism'), max_length=255, blank=True)
    middle_name = models.CharField(verbose_name=_('Otasining ismi'), max_length=255, blank=True)
    role = models.CharField(verbose_name=_('Foydalanuvchi roli'), choices=ROLE, max_length=15, default=USER)
    region = models.ForeignKey(Region, verbose_name=_('Viloyat'), on_delete=models.SET_NULL, null=True, blank=True)
    district = models.ForeignKey(District, verbose_name=_('Tuman/Shahar'), on_delete=models.SET_NULL, null=True,
                                 blank=True)
    quarter = models.ForeignKey(Quarter, on_delete=models.SET_NULL, verbose_name=_('Mahalla'), null=True, blank=True)
    address = models.CharField(verbose_name=_("Manzil"), max_length=255, blank=True, null=True)
    email = models.EmailField(verbose_name=_("e-mail"), max_length=254, unique=False, blank=True, default='')
    birthday = models.DateField(blank=True, verbose_name=_("Tug'ilgan vaqti"), null=True, default=timezone.now)
    username = models.CharField(max_length=30, unique=True, blank=True)
    phone = models.IntegerField(verbose_name=_('Telefon raqam'), null=True, unique=True,
                                validators=[MaxValueValidator(999999999), MinValueValidator(100000000)])
    passport_seriya = models.CharField(verbose_name=_("Passport seriyasi"), max_length=10, null=True, blank=True)
    passport_number = models.IntegerField(verbose_name=_("Passport raqami"), null=True, blank=True)
    person_id = models.BigIntegerField(verbose_name=_('JShShIR'), blank=True, null=True, )
    issue_by_whom = models.CharField(verbose_name=_('Passport kim tomonidan berilgan'), max_length=30, blank=True,
                                     null=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False, blank=True)
    is_active = models.BooleanField(verbose_name=_("Faolligi"), default=True, blank=True)
    last_login = models.DateTimeField(null=True, auto_now=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    gender = models.IntegerField(verbose_name=_('Jinsi'), choices=GENDER_CHOICES, default=MEN, blank=True, null=True)
    turbo = models.CharField(max_length=200, blank=True, null=True, validators=[MinLengthValidator(5)])

    USERNAME_FIELD = 'username'

    objects = UserManager()

    def __str__(self):
        if self.last_name and self.first_name:
            return f"{self.last_name} {self.first_name}"
        else:
            return self.username


class UserManager(BaseUserManager):

    def _create_user(self, email, username, password, is_staff, is_superuser, **extra_fields):
        if not email:
            raise Http404
        if not username:
            raise Http404
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            username=username,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password, **extra_fields):
        return self._create_user(username, password, False, False, **extra_fields)

    def create_superuser(self, email, username, password, **extra_fields):
        user = self._create_user(email, username, password, True, True, **extra_fields)
        user.save(using=self._db)
        return user


class Organization(models.Model):
    director = models.ForeignKey(User, on_delete=models.CASCADE, related_name='organization_director')
    title = models.CharField(verbose_name="Korxona nomi", max_length=255)
    inn = models.IntegerField(verbose_name="Korxona INN raqami", null=True, blank=True)



    def __str__(self):
        return self.title
