from urllib import request
import requests
from django.db import models

from django.utils import timezone


from user.models import *
from service.models import *


# davlat bojlari
class StateDuty(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE, related_name="state_duty_with_service")
    title = models.CharField(max_length=255)
    key = models.CharField(unique=True, max_length=255)

    def __str__(self):
        return self.title


# Davlat bojlari hisob raqamlari
class StateAccountNumber(models.Model):
    state_duty = models.ForeignKey(StateDuty, on_delete=models.CASCADE)
    district = models.ForeignKey(District, verbose_name='Tuman/Shahar', on_delete=models.CASCADE)
    account_number = models.CharField(verbose_name='Hisob raqami', default=0, max_length=40,unique=True)
    created_date = models.DateTimeField(auto_now_add=True, max_length=30)
    updated_date = models.DateTimeField(auto_now=True, max_length=30, blank=True, null=True)

    def __str__(self):
        return self.state_duty.title


# BHMga nisbatan to'lanadigan davlat boji
class StateDutyPercent(models.Model):
    state_duty = models.ForeignKey(StateDuty, on_delete=models.CASCADE, related_name="state_duty_percent_with_state_duty")
    made_year_start = models.IntegerField(default=0)
    made_year_stop = models.IntegerField(default=0)
    percent = models.FloatField(default=0)
    created_date = models.DateTimeField(auto_now_add=True, max_length=30, blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, max_length=30, blank=True, null=True)

    def __str__(self):
        return f"{self.state_duty.title} : {self.percent}%"

    class Meta:
        verbose_name = 'Davlat boji foizlari'
        verbose_name_plural = 'Davlat bojlari foizlari'


class StateDutyPayment(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    state_duty = models.ForeignKey(StateDuty, on_delete=models.CASCADE)
    payment = models.IntegerField(verbose_name="To'lovi", default=0)
    created_date = models.DateTimeField(auto_now_add=True, max_length=30, blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, max_length=30, blank=True, null=True)
    is_paid = models.BooleanField(default=False, verbose_name="To'langanligi")
    account_number = models.ForeignKey(StateAccountNumber, verbose_name='Hisob raqam', on_delete=models.CASCADE,
                                       related_name="state_account_number")
    is_active = models.BooleanField(default=True)
    is_free = models.BooleanField(default=True)

    def __str__(self):
        return self.application.created_user.username
