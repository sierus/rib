from django.contrib import admin
from payment.models import *


# Register your models here.
class StateAccountNumberAdmin(admin.StackedInline):
    model = StateAccountNumber
    extra = 5

    # max_num = 1


@admin.register(StateDutyPercent)
class StateDutyPercentAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ['id', 'state_duty', 'made_year_start', 'made_year_stop','percent']
    list_display_links = ['id', 'state_duty']


@admin.register(StateDuty)
class StateDutyAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    list_display_link = ['id', 'title']
    save_on_top = True
    inlines = [StateAccountNumberAdmin]


admin.site.register(StateDutyPayment)