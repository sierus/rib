from django.db import models


# Create your models here.
from user.models import *



class Service(models.Model):
    service_id = models.IntegerField(verbose_name="Servis ID", unique=True)
    title = models.CharField(verbose_name="Xizmat nomi", max_length=255)
    desc = models.TextField()
    image = models.CharField(verbose_name="Foto", blank=True, max_length=255)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Xizmat'
        verbose_name_plural = 'Xizmatlar'


CREATED = 0 #Yaratildi
SHIPPED = 1  #Jo'natildi
ACCEPTED_FOR_CONSIDERATION = 2  # Ko'rib chiqish uchun qabul qilindi
WAITING_FOR_PAYMENT = 3
WAITING_FOR_ORIGINAL_DOCUMENTS = 4
SUCCESS = 5
REJECTED = 6


PROCESS_CHOICES = (
    (CREATED, "Ariza yaratildi"),
    (SHIPPED, "Jo'natildi"),
    (ACCEPTED_FOR_CONSIDERATION, "Ko'rib chiqish uchun qabul qilindi"),
    (WAITING_FOR_PAYMENT, "To'lovni kutmoqda"),
    (WAITING_FOR_ORIGINAL_DOCUMENTS, "Hujjatlarni asl nusxasini kutmoqda"),
    (SUCCESS, "Muvaffaqiyatli yakunlandi"),
    (REJECTED, "Rad etildi"),

)
CRON_CHOICES = (
    ('1', '3 days inside'),
    ('2', '7 days before'),
    ('3', '7 days after'),
)


class Application(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    created_user = models.ForeignKey(User, verbose_name='Yaratgan shaxs', on_delete=models.SET_NULL, null=True)
    created_date = models.DateTimeField(default=timezone.now, max_length=30)
    updated_date = models.DateTimeField(default=timezone.now, max_length=30)
    process = models.IntegerField(choices=PROCESS_CHOICES, max_length=10, verbose_name="Ariza holati", default=CREATED)
    is_payment = models.BooleanField("To\'lov qilingan", default=False)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, blank=True, null=True)
    password = models.IntegerField(blank=True, null=True, verbose_name='Ariza tekshiruv kodi')
    given_date = models.DateField('Berish sanasi', blank=True, null=True)
    given_time = models.CharField('Berish vaqti', max_length=10, blank=True, null=True)
    canceled_date = models.DateTimeField(max_length=30, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_block = models.BooleanField(default=True)
    cron = models.CharField(choices=CRON_CHOICES, max_length=15, verbose_name="CRON holati", default=1)
    department = models.ForeignKey(Department, verbose_name="Ariza topshirilgan IIB YHXB bo'limi",
                                   on_delete=models.CASCADE, blank=True, null=True,
                                   related_name='application_section')

    class Meta:
        verbose_name = 'Ariza'
        verbose_name_plural = 'Arizalar'
        ordering = ['-id']

    def __str__(self):
        return f"Application object: {self.id}"


NUMBER = 0
CHECKBOX = 1
TEXT = 2
TEXTAREA = 3
RADIO = 4
DATE = 5
DATE_TIME = 6
SELECT = 7
MANY_SELECT = 8

FIELD_TYPE_CHOICES = (
    (NUMBER, "number"),
    (CHECKBOX, "Checkbox"),
    (TEXT, "Text"),
    (TEXTAREA, "Textarea"),
    (RADIO, "Radio"),
    (DATE, "Date"),
    (DATE_TIME, "DateTime"),
    (SELECT, "Select"),
    (MANY_SELECT, "ManyToManySelect"),
)


class ServiceSelect(models.Model):
    slug = models.CharField(max_length=20)
    title = models.CharField(verbose_name="Nomi", max_length=255)  # Masalan Avtomobil nomi

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Forma uchun select"
        verbose_name_plural = "Forma uchun select"


class ServiceSelectItems(models.Model):
    service_select = models.ForeignKey(ServiceSelect, on_delete=models.CASCADE, related_name='service_select_items')
    value = models.CharField(verbose_name="Qiymati", unique=True, max_length=255)



    def __str__(self):
        return self.value

class ServiceInputField(models.Model):
    service = models.ManyToManyField(Service, verbose_name="Xizmatlar", related_name='service_input_field')
    # slug = models.CharField(verbose_name="Qo'llaniladigan nomi", max_length=20)
    title = models.CharField(verbose_name="Nomi", max_length=255)  # Masalan Davlat raqam belgisi
    field_name = models.CharField(verbose_name="Field nomi", max_length=225)
    placeholder = models.CharField(max_length=255, blank=True)
    field_type = models.IntegerField(verbose_name="Field turi", null=True, blank=True, choices=FIELD_TYPE_CHOICES)
    select = models.ForeignKey(ServiceSelect, on_delete=models.CASCADE, blank=True, null=True, related_name="select_input_field")
    max_length = models.IntegerField(verbose_name="Max Length", blank=True, null=True)
    sort = models.IntegerField(default=1)
    is_required = models.BooleanField(default=True)
    div_form_group = models.BooleanField(default=True)
    div_class = models.CharField(max_length=255, blank=True)
    css_class = models.CharField(max_length=255, blank=True)
    css_code = models.TextField(blank=True)
    js_code = models.TextField(blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Forma yaratish"
        verbose_name_plural = "Forma yaratishlar"
        ordering = ['sort']

class Document(models.Model):
    title = models.CharField(max_length=255, verbose_name='Hujjat nomi')

    def __str__(self):
        return self.title


class ServiceInfo(models.Model):
    service = models.ForeignKey(Service, verbose_name="Xizmat", on_delete=models.CASCADE, related_name='service_info')
    deadline = models.CharField(verbose_name="Muddati", max_length=20, )
    how_it_works = models.TextField()
    document = models.ManyToManyField(Document, related_name='service_documents')

    def __str__(self):
        return self.service.title


class ServiceDataSave(models.Model):
    # application = models.ForeignKey(Application, on_delete=models.CASCADE, )
    field = models.TextField()
    data = models.TextField()


class SaveDataForm(models.Model):
    items = models.ForeignKey(ServiceInputField, on_delete=models.CASCADE)
    data = models.CharField(max_length=255)
    application = models.ForeignKey(Application, verbose_name='Application', on_delete=models.CASCADE, related_name='save_from_data_with_application')


class StepByStep(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE, related_name='step_by_step_with_service')

    def __str__(self):
        return f"StepByStep: {self.service.title}"


class Step(models.Model):
    step_by_step = models.ForeignKey(StepByStep, on_delete=models.CASCADE, related_name='step_with_step_by_step')
    input_fields = models.ManyToManyField(ServiceInputField, related_name='step_with_service_fields', blank=True)
    title = models.CharField(max_length=255)
    sort = models.IntegerField(blank=True, null=True)
    css_id = models.CharField(max_length=255, blank=True)
    icon = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return f"Steps: {self.title}"

    class Meta:
        ordering = ['sort']

USER_TYPE = 0
FORM = 1
PAYMENT_FOR_APP = 2
PAYMENT_STATE_DUTY = 3

ELEMENT_TYPE_CHOICES = (
    (USER_TYPE, "USER_TYPE"),
    (FORM, "FORM"),
    (PAYMENT_FOR_APP, "PAYMENT_FOR_APP"),
    (PAYMENT_STATE_DUTY, "PAYMENT_STATE_DUTY"),

)


class StepElement(models.Model):
    step = models.ForeignKey(Step, on_delete=models.CASCADE, related_name='step_elements')
    type_element = models.IntegerField(verbose_name="Element turi", choices=ELEMENT_TYPE_CHOICES)

