from django.urls import path, include
from service.views import *


app_name = 'service'

urlpatterns = [
    path('', ServicesList.as_view(), name='services_list'),
    path('service/info/<int:pk>/', ServiceInfo.as_view(), name='service_info'),
    path('service/save-data/', SaveData.as_view(), name='save_data'),
    path('service/step-by-step/<int:pk>/', StepByStep.as_view(), name='step_by_step'),
    path('service/save-data-succes/', SaveDataSuccess.as_view(), name='save_data_success'),
    path('applications/list/', ApplicationsList.as_view(), name='applications_list'),
    path('application/detail/<int:pk>', ApplicationDetail.as_view(), name='application_detail'),
    path('get-car-data/', GetCarData.as_view(), name='get_car_data'),
]
