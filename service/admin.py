import socketserver

from django.contrib import admin

# Register your models here.
from service.models import *


class ServiceInfoAdmin(admin.StackedInline):
    model = ServiceInfo
    can_delete = False
    extra = 1
    max_num = 1


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    inlines = [ServiceInfoAdmin, ]


# admin.site.register(Rule,RuleAdmin)
# @admin.register(Service)
# class ServiceAdmin(admin.ModelAdmin):
#     list_display = ['id', 'title']
#     list_display_links = ['id', 'title']
#     save_on_top = True

#
# @admin.register(ServiceSelect)
# class ServiceSelectAdmin(admin.ModelAdmin):
#     list_display = ['id', 'title']
#     list_display_links = ['id', 'title']
#     save_on_top = True
#
#
# @admin.register(ServiceSelectItems)
# class ServiceSelectItemsAdmin(admin.ModelAdmin):
#     list_display = ['id', 'key', 'value']
#     list_display_links = ['id', 'key', 'value']
#     save_on_top = True


class ServiceSelectItemsAdmin(admin.StackedInline):
    model = ServiceSelectItems
    # can_delete = False
    extra = 10
    # max_num = 1


@admin.register(ServiceSelect)
class ServiceSelectAdmin(admin.ModelAdmin):
    inlines = [ServiceSelectItemsAdmin, ]


@admin.register(ServiceInputField)
class ServiceInputFieldAdmin(admin.ModelAdmin):
    list_display = ['id', 'title','sort']
    list_display_links = ['id', 'title', ]
    save_on_top = True


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    list_display_links = ['id', 'title']
    save_on_top = True


@admin.register(SaveDataForm)
class SaveDataFormAdmin(admin.ModelAdmin):
    list_display = ['id', 'items']
    list_display_links = ['id', 'items']
    save_on_top = True


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['id', 'created_user']
    list_display_links = ['id', 'created_user']
    save_on_top = True


# @admin.register(Service):
# class ServiceAdmin(models.Model):

@admin.register(StepByStep)
class StepByStepAdmin(admin.ModelAdmin):
    list_display = ['id', 'service']
    list_display_links = ['id', 'service']
    save_on_top = True


class StepElementAdmin(admin.StackedInline):
    model = StepElement
    # can_delete = False
    extra = 10
    # max_num = 1


@admin.register(Step)
class StepAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'sort']
    list_display_link = ['id', 'title', 'sort']
    inlines = [StepElementAdmin]



