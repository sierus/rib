import datetime
import json
import re

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework.authtoken.models import Token
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ImproperlyConfigured
from django.db.models import QuerySet, Q
from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from django.core.exceptions import PermissionDenied
from service.models import *
from datetime import datetime as dt
from django.views.generic.base import *


@permission_classes([IsAuthenticated])
class ApplicationCustomMixin(ListView):
    render_application_values = []
    model = None
    request = None

    # def __init__(self, *args, **kwargs):
    #     super(ApplicationsList, self).__init__(*args, **kwargs)

    # def get_template_names(self):
    #     print(self.request.user.role)
    #     if self.request.user.role == STATE_CONTROLLER:
    #         template_name = 'user/role/state_controller/applications_list.html'
    #     elif self.request.user.role == USER:
    #         template_name = 'application/applications_list.html'
    #     else:
    #         template_name = self.template_name
    #     return [template_name]

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context.update(foo='bar')
    #     context.update(object_list=self.get_queryset())
    #     return context

    # def dispatch(self, request, *args, **kwargs):
    #    #allow user to call this View if their Client owns the Survey
    #    self.survey = get_object_or_404(Survey, hash=self.kwargs['hash'])
    #    up = get_object_or_404(UserProfile, fk_user=self.request.user)
    #    self.client = up.fk_client
    #    if self.survey.fk_client != self.client:
    #        raise Http404
    #    return super(AjaxQuestionList, self).dispatch(request, *args, **kwargs)

    # @allowed_users(allowed_roles=[*allowed_roles])
    # def dispatch(self, *args, **kwargs):
    #     return super().dispatch(*args, **kwargs)



    def myconverter(self, obj):
        if isinstance(obj, (datetime.datetime)):
            return obj.strftime("%d.%m.%Y %H:%M").__str__()
        elif isinstance(obj, (datetime.date)):
            return obj.strftime("%d.%m.%Y").__str__()

    def get_choices_value(self, q, choices):
        choices_list = [key for key, value in choices if re.search(q.lower(), value.lower())]
        # refund_dict = {key: value for key, value in SERVICE_CHOICES}
        return choices_list

    def get_applications_values(self, queryset):
        if self.render_application_values and queryset is not None:
            qs = queryset.values(*self.render_application_values)
            return qs
        else:
            raise Http404("get_applications_values is empty or queryset is None")

    def get_queryset(self):
        # id_list = []
        # for item in super().get_queryset():
        #     if item.section.pay_for_service and not item.is_block:
        #         id_list.append(item.id)
        #     elif not item.section.pay_for_service:
        #         id_list.append(item.id)
        # qs = super().get_queryset().filter(id__in=id_list, is_active=True)

        q = self.request.GET.get('q', '').lower()
        order_by = self.request.GET.get('order_by', 'created_date')
        date_pattern = '^[0-9]{2}.[0-9]{2}.[0-9]{4}$'
        day_pattern = '^[0-9]{2}$|^[0-9]{2}.$'
        day_and_month_pattern = '^[0-9]{2}.[0-9]{2}$|^[0-9]{2}.[0-9]{2}.$'
        print(q)
        qs = self.model.objects.filter(
            Q(Q(id=q) if q.isdigit() else Q()) |
            Q(service__title__icontains=q) |
            Q(created_user__first_name__icontains=q) |
            Q(created_user__last_name__icontains=q) |
            Q(created_user__middle_name__icontains=q) |
            Q(organization__title__icontains=q) |
            Q(process__in=self.get_choices_value(q, PROCESS_CHOICES)) |
            # filter by date_pattern
            Q(Q(created_date__date=dt.strptime(q[0:10], '%d.%m.%Y').date()) if re.match(date_pattern, q) else Q()) |
            # filter by day_pattern
            Q(Q(created_date__day=q[0:2]) if re.match(day_pattern, q) else Q()) |
            # filter by day_and_month_pattern
            Q(Q(Q(created_date__day=q[0:2]) & Q(created_date__month=q[3:5])) if re.match(day_and_month_pattern,
                                                                                         q) else Q())
        ).order_by(f"-{order_by}")

        return qs


    def get_json_data(self):
        start = int(self.request.GET.get('start'))
        finish = int(self.request.GET.get('limit'))

        qs = self.get_queryset()
        data = self.get_applications_values(qs)

        list_data = []
        for index, item in enumerate(data[start:start + finish], start):
            application = get_object_or_404(self.model, id=item['id'])
            try:
                item['created_date'] = self.myconverter(item['created_date'])
                item['service'] = get_object_or_404(Service, id=item['service']).title
                item['created_user'] = application.organization.title if application.organization else f"{application.created_user.first_name} {application.created_user.last_name} {application.created_user.middle_name}" if application.created_user.last_name and application.created_user.first_name else application.created_user.username
            except:
                pass
            list_data.append(item)

        context = {
            'length': data.count(),
            'objects': list_data
        }

        data = json.dumps(context)
        return HttpResponse(data, content_type='json')


@method_decorator(login_required, name='dispatch')
class AllowedRolesMixin(LoginRequiredMixin, View):
    allowed_roles = None

    def dispatch(self, *args, **kwargs):
        if not self.request.user.role in self.allowed_roles:
            raise Http404

        # try:
        #     token = self.request.COOKIES.get('token')
        #     Token.objects.get(key=token)
        # except ObjectDoesNotExist:
        #     return redirect(reverse_lazy('user:custom_logout'))

        return super().dispatch(*args, **kwargs)