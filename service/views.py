import datetime

import requests
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
# @login_required
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView

from rib.settings import MINIMUM_BASE_WAGE
from service.mixins import ApplicationCustomMixin, AllowedRolesMixin
from service.models import *


class ServicesList(ListView):
    model = Service
    template_name = 'rib/services/services_list.html'
    ordering = ['id']

    context_object_name = 'services'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        qs = super().get_queryset().filter(is_active=True)
        return qs


class ServiceInfo(DetailView):
    model = Service
    template_name = 'rib/services/service_info.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context.update(form_fields=ServiceInputField.objects.filter(service=self.kwargs.get('pk')))
        context.update(NUMBER=NUMBER)
        context.update(CHECKBOX=CHECKBOX)
        context.update(TEXT=TEXT)
        context.update(TEXTAREA=TEXTAREA)
        context.update(RADIO=RADIO)
        context.update(DATE=DATE)
        context.update(DATE_TIME=DATE_TIME)
        context.update(SELECT=SELECT)

        return context


class SaveData(View):
    def post(self, request, *args, **kwargs):
        service = Service.objects.get(pk=request.POST.get('service'))
        application = Application.objects.create(created_user=request.user, service=service)
        print(request.POST)

        for key, values in request.POST.lists():
            # print(key, ': ', values)
            if key != 'csrfmiddlewaretoken' and key != 'service':
                if key == 'person_type' and values == ['1']:
                    organization = Organization.objects.get(pk=request.POST.get('organization'))
                    Application.objects.filter(pk=application.pk).update(organization=organization)
                try:
                    item = ServiceInputField.objects.filter(field_name=key).first()
                except ServiceInputField.DoesNotExist:
                    item = None
                if key != 'person_type' and key != 'organization':
                    # print(key, 67)
                    is_many_select = ServiceInputField.objects.filter(field_name=key).first()
                    # print(is_many_select, 69)
                    if is_many_select.field_type == MANY_SELECT and item is not None:

                        for value in values:
                            # get_item = ServiceSelectItems.objects.filter(id=value).first()

                            save_data = SaveDataForm.objects.create(items=is_many_select, data=value,
                                                                    application=application)
                    else:
                        values = str(values).replace("[", "").replace("]", "").replace("'", "")
                        save_data = SaveDataForm.objects.create(items=item, data=values, application=application)

        return redirect(reverse_lazy('service:save_data_success'))


class SaveDataSuccess(LoginRequiredMixin, View):
    template_name = 'rib/services/save_form_success.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class StepByStep(LoginRequiredMixin, DetailView):
    model = StepByStep
    template_name = 'rib/services/step_by_step.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(USER_TYPE=USER_TYPE)
        context.update(FORM=FORM)
        context.update(PAYMENT_STATE_DUTY=PAYMENT_STATE_DUTY)
        context.update(PAYMENT_FOR_APP=PAYMENT_FOR_APP)
        context.update(NUMBER=NUMBER)
        context.update(CHECKBOX=CHECKBOX)
        context.update(TEXT=TEXT)
        context.update(TEXTAREA=TEXTAREA)
        context.update(RADIO=RADIO)
        context.update(DATE=DATE)
        context.update(DATE_TIME=DATE_TIME)
        context.update(SELECT=SELECT)
        context.update(MANY_SELECT=MANY_SELECT)
        return context


class ApplicationsList(AllowedRolesMixin, ApplicationCustomMixin):
    model = Application
    template_name = 'rib/application/applications_list.html'
    allowed_roles = [USER, REVIEWER, CHECKER, TECHNICAL, DISTRICAL_CONTROLLER, REGIONAL_CONTROLLER, STATE_CONTROLLER,
                     OPERATOR, MODERATOR, ADMINISTRATOR, SUPER_ADMINISTRATOR]
    render_application_values = ['id', 'service', 'created_user', 'created_date', 'process']

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            return self.get_json_data()
        else:
            return super().get(self, request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = {
            'saved_datas': list(),
            'application_list': self.object_list
        }
        for application in self.get_queryset():

            saved_datas = SaveDataForm.objects.filter(application=application)
            if saved_datas.exists():
                context['saved_datas'].append(saved_datas)
                context['saved_datas'].append(application.process)
        return context


class ApplicationDetail(DetailView):
    model = Application
    template_name = 'rib/application/application_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        steps = Step.objects.filter(step_by_step__service__application__pk=self.kwargs.get('pk'))
        context.update(steps=steps)
        made_year = SaveDataForm.objects.get(application_id=self.kwargs.get('pk'), items__field_name='made_year')
        this_time = datetime.datetime.now()
        age = this_time.year - int(made_year.data)

        context.update(age=age)
        context.update(MINIMUM_BASE_WAGE=MINIMUM_BASE_WAGE)
        return context


class GetCarData(View):
    def post(self, request, *args, **kwargs):
        return HttpResponse(self.get_context_data(), content_type='json')

    def get_context_data(self):
        context = dict()
        application = Application.objects.filter(id=self.request.POST.get('app_id')).first()
        save_data = SaveDataForm.objects.filter(application=application, items__field_name='car_model').first()
        if save_data:
            car = ServiceInputField.objects.filter(id=save_data.items.id).first()
            print(car)

        else:
            pass

        return context
