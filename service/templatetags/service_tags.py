from django import template
from django.shortcuts import get_object_or_404

from service.models import Application, SaveDataForm

register = template.Library()


@register.simple_tag
def get_car_data(app_id):
    print(app_id)
    application = Application.objects.filter(id=app_id).first()
    saved_datas = SaveDataForm.objects.filter(application=application)
    for save_data in saved_datas:
        print(save_data)
    return 'good'


@register.simple_tag
def get_state_duty_sum(min_base, percent):
    sum = (min_base / 100 ) * percent
    return sum
